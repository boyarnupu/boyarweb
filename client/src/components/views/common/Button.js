import React from "react";

class Button extends React.Component {
    
    render(){
        let elementClass = "button";
        elementClass += this.props.className ? " " + this.props.className : "";
        return (
            <button className={elementClass}>{this.props.children}</button>
        );
    }
}

export default Button;