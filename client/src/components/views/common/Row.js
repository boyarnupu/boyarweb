import React from "react";

class Row extends React.Component {
    
    render(){
        let elementClass = "row"
        elementClass += this.props.className ? " " + this.props.className : "";
        return (
            <div className={elementClass}>{this.props.children}</div>
        );
    }
}

export default Row;