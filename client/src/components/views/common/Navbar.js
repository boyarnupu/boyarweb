import React from "react";
import { Link, NavLink } from "react-router-dom";
import Container from './../common/Container.js';
import siteLogo from './../../../assets/img/logo.png';

class Navbar extends React.Component {

    render(){
        return (
            <nav className="navbar">
                <Container className="navbar__container">
                    <div className="navbar__logo">
                        <Link to="/"><img src={siteLogo} alt="Boyarweb Logo"/></Link>
                    </div>
                    <div className="navbar__nav-menu">
                        <NavLink className="navbar__nav-link" to="/" activeClassName="active">Home</NavLink>
                    </div>
                </Container>
            </nav>
        );
    }
}

export default Navbar;