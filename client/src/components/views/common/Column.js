import React from "react";

class Column extends React.Component {
    
    render(){
        let elementClass = "column"
        elementClass += this.props.className ? " " + this.props.className : "";
        return (
            <div className={elementClass}>
                <div className="column__inner">{this.props.children}</div>
            </div>
        );
    }
}

export default Column;