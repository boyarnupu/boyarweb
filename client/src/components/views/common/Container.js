import React from "react";

class Container extends React.Component {
    
    render(){
        let elementClass = "container"
        elementClass += this.props.className ? " " + this.props.className : "";
        return (
            <div className={elementClass}>{this.props.children}</div>
        );
    }
}

export default Container;