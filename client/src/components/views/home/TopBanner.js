import React from "react";
import './../../../assets/css/home/top-banner.scss';
import Separator from './../common/Separator.js';
import Button from './../common/Button.js';

function TopBanner(props){

    return (
        <div className="top-banner">
            <div className="top-banner__inner">
                <div className="top-banner__headers">
                    <h1 className="top-banner__header">Boyar<span className="top-banner__header__span">web</span></h1>
                    <Separator></Separator>
                    <h2 className="top-banner__subheader">Web-development studio</h2>
                    <Button className="button_primary">GET STARTED NOW</Button>
                    <Button className="button_outline">LEARN MORE</Button>
                </div>
            </div>
            <div className="top-banner__arrow"></div>
        </div>
    );

}

export default TopBanner;