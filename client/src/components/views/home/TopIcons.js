import React from "react";
import './../../../assets/css/home/about-us.scss';
import topIconImage1 from './../../../assets/img/top-icon-1.png';
import topIconImage2 from './../../../assets/img/top-icon-2.png';
import topIconImage3 from './../../../assets/img/top-icon-3.png';
import topIconImage4 from './../../../assets/img/top-icon-4.png';
import Container from './../common/Container.js';

function TopIcons(props){

    return (
        <div className="top-icons">
            <Container className="top-icons__container">
                <IconBox iconImage={topIconImage1} iconTitle="SLEEK DESIGN" iconDescription="Sleek design description"/>
                <IconBox iconImage={topIconImage2} iconTitle="CLEAN CODE" iconDescription="Clean code description"/>
                <IconBox iconImage={topIconImage3} iconTitle="CREATIVE IDEAS" iconDescription="Creative ideas description"/>
                <IconBox iconImage={topIconImage4} iconTitle="FREE SUPPORT" iconDescription="Free support description"/>
            </Container>
        </div>
    );

}

function IconBox(props){

    return (
        <div className="top-icons__icon-box">
            <div className="top-icons__icon-box__icon-frame">
                <div className="top-icons__icon-box__icon" style={{ backgroundImage: 'url(' + props.iconImage +') ' }}></div>
            </div>
            <h3 className="heading heading_h3">{props.iconTitle}</h3>
            <div className="top-icons__icon-box__separator"></div>
            <p>{props.iconDescription}</p>
        </div>
    );

}

export default TopIcons;