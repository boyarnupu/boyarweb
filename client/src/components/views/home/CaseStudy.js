import React from "react";
import './../../../assets/css/home/case-study.scss';
import caseStudyImage from './../../../assets/img/case-study.jpg';
import Separator from './../common/Separator.js';
import Container from './../common/Container.js';
import Row from './../common/Row.js';
import Column from './../common/Column.js';
import Button from './../common/Button.js';

function CaseStudy(props){

    return (
        <div className="case-study">
            <Container className="case-study__container">
                <h2 className="heading heading_h2 align-center">Case Study</h2>
                <Separator />
                <Row className="row_vertical-center case-study__row">
                    <Column className="align-center case-study__left-column">
                        <div className="case-study__icon-frame"><div className="case-study__icon"></div></div>
                        <h3 className="heading heading_h3">ACCUMULATE CREATIVE IDEAS</h3>
                        <div className="case-study__text-separator"></div>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ip sum has been the industry's standard dummy text ever since the 1500s, when an unk- nown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        <Button className="button_primary ">Read More</Button>
                    </Column>
                    <Column>
                        <img src={caseStudyImage} alt="About Boarweb"/>
                    </Column>
                </Row>
            </Container>
        </div>
    );

}

export default CaseStudy;