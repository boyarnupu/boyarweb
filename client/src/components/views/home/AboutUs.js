import React from "react";
import './../../../assets/css/home/about-us.scss';
import aboutUsImage from './../../../assets/img/about-us.png';
import Button from './../common/Button.js';
import Container from './../common/Container.js';
import Row from './../common/Row.js';
import Column from './../common/Column.js';

function AboutUs(props){

    return (
        <div className="about">
            <Container className="about__container">
                <Row className="row_vertical-center">
                    <Column>
                        <img src={aboutUsImage} alt="About Boarweb"/>
                    </Column>
                    <Column>
                        <h2 className="heading heading_h2 vertical-line-before">Our History</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ip sum has been the industry's standard dummy text ever since the 1500s, when an unk- nown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        <Button className="button_primary ">BROWSE OUR WORK</Button>
                    </Column>
                </Row>
            </Container>
        </div>
    );

}

export default AboutUs;