import React from "react";
import './../../../assets/css/home/pricing.scss';
import Separator from './../common/Separator.js';
import Container from './../common/Container.js';
import Row from './../common/Row.js';
import Button from './../common/Button.js';

function Pricing(props){

    return (
        <div className="pricing">
            <Container className="pricing__container">
                <h2 className="heading heading_h2 align-center">Our Pricing</h2>
                <Separator />
                <Row className="pricing__row">
                    <PricingBox pricingTitle="Starter" pricingPrice="10"/>
                    <PricingBox pricingTitle="Premium" pricingPrice="20"/>
                    <PricingBox pricingTitle="Business" pricingPrice="30"/>
                </Row>
            </Container>
        </div>
    );

}

function PricingBox(props){

    return (
        <div className="pricing-box">
            <div className="pricing-box__half-overlay"></div>
            <div className="pricing-box__inner">
                <h3 className="heading heading__h3 text-white">{props.pricingTitle}</h3>
                <div className="pricing-box__price-wrapper">
                    <div className="pricing-box__price">$<span>{props.pricingPrice}</span></div>
                </div>
                <Button className="pricing__button">Choose</Button>
            </div>
        </div>
    );

}


export default Pricing;