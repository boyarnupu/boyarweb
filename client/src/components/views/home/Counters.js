import React from "react";
import './../../../assets/css/home/counters.scss';
import topIconImage1 from './../../../assets/img/top-icon-1.png';
import topIconImage2 from './../../../assets/img/top-icon-2.png';
import topIconImage3 from './../../../assets/img/top-icon-3.png';
import topIconImage4 from './../../../assets/img/top-icon-4.png';
import Container from './../common/Container.js';

function Counters(props){

    return (
        <div className="counters">
            <div className="counters__overlay"></div>
            <Container className="counter__container">
                <Counter iconImage={topIconImage1} iconTitle="SLEEK DESIGN" iconDescription="Sleek design description"/>
                <Counter iconImage={topIconImage2} iconTitle="CLEAN CODE" iconDescription="Clean code description"/>
                <Counter iconImage={topIconImage3} iconTitle="CREATIVE IDEAS" iconDescription="Creative ideas description"/>
                <Counter iconImage={topIconImage4} iconTitle="FREE SUPPORT" iconDescription="Free support description"/>
            </Container> 
        </div>
    );

}

function Counter(props){

    return (
        <div className="counter">
            <div className="counter__icon-wrapper">
                <div className="counter__icon" style={{ backgroundImage: 'url(' + props.iconImage +') ' }}></div>
            </div>
            <h3 className="heading heading_h3">{props.iconTitle}</h3>
            <p>{props.iconDescription}</p>
        </div>
    );

}



export default Counters;