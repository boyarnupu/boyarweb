import React from "react";
import './../../../assets/css/home/portfolio.scss';
import portfolioImage1 from './../../../assets/img/portfolio-1.jpg';
import portfolioImage2 from './../../../assets/img/portfolio-2.jpg';
import portfolioImage3 from './../../../assets/img/portfolio-3.jpg';
import Separator from './../common/Separator.js';

function Portfolio(props) {

    return (
        <div className="portfolio">
            <h2 className="heading heading_h2 align-center">Our Works</h2>
            <Separator />
            <PortfolioGallery />
        </div>
    );

}

function PortfolioGallery(props) {

    return (
        <div className="portfolio__gallery">
            <PortfolioItem portfoiloImage={portfolioImage1} portfolioTitle="Site 1" />
            <PortfolioItem portfoiloImage={portfolioImage2} portfolioTitle="Site 2" />
            <PortfolioItem portfoiloImage={portfolioImage3} portfolioTitle="Site 3" />
        </div>
    );

}

function PortfolioItem(props) {

    return (
        <div className="portfolio__item">
            <img  className="portfolio__item__image" src={props.portfoiloImage} alt={props.portfolioTitle}/>
        </div>
    );

}

export default Portfolio;