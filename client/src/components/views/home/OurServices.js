import React from "react";
import './../../../assets/css/home/our-services.scss';
import serviceIconImage1 from './../../../assets/img/service-icon-1.png';
import serviceIconImage2 from './../../../assets/img/service-icon-2.png';
import serviceIconImage3 from './../../../assets/img/service-icon-3.png';
import Row from './../common/Row.js';
import Column from './../common/Column.js';

function OurServices(props) {

    return (
        <div className="our-services">
            <Row className="our-services__row">
                <Column className="column__left">
                    <h2 className="heading heading_h2 vertical-line-after">Our Services</h2>
                    <div className="our-services__services">
                        <Service serviceImage={serviceIconImage1} serviceTitle="Web Design" serviceDescription="Web Design description" />
                        <Service serviceImage={serviceIconImage2} serviceTitle="Print Design" serviceDescription="Print Design description" />
                        <Service serviceImage={serviceIconImage3} serviceTitle="Photography" serviceDescription="Photography description" />
                    </div>
                </Column>
                <Column className="column__right" />
            </Row>
        </div>
    );

}

function Service(props) {

    return (
        <div className="our-services__service">
            <h3 className="heading heading_h3">{props.serviceTitle}</h3>
            <p>{props.serviceDescription}</p>
            <div className="service__icon-wrapper">
                <div className="service__icon" style={{ backgroundImage: 'url(' + props.serviceImage + ') ' }}></div>
            </div>
        </div>
    );

}

export default OurServices;