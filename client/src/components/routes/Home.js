import React, { Component } from 'react';
import TopBanner from '../views/home/TopBanner';
import TopIcons from '../views/home/TopIcons';
import AboutUs from '../views/home/AboutUs';
import OurServices from '../views/home/OurServices';
import Portfolio from '../views/home/Portfolio';
import CaseStudy from '../views/home/CaseStudy';
import Counters from '../views/home/Counters';
import Pricing from '../views/home/Pricing';

class Home extends Component {
    render() {
        return (
            <div id="home">
                <TopBanner />
                <TopIcons />
                <AboutUs />
                <OurServices />
                <Portfolio />
                <CaseStudy />
                <Counters />
                <Pricing />
            </div>
        );
    }
}

export default Home;
