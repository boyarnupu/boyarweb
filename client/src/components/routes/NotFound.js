import React from 'react';

function NotFound({ location }) {
    return (
        <div id="not-found">
            404 for {location.pathname}
        </div>
    );
}

export default NotFound;
