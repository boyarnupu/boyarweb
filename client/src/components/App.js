import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Navbar from './views/common/Navbar';
import Home from './routes/Home';
import NotFound from './routes/NotFound';

class App extends React.Component {

  render() {

    return (
      <Router>
        <div className="page-wrapper">
          <Navbar />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route component={NotFound} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
